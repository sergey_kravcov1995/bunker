const userStatus = {
    online: 1,
    refresh: 2,
    disconnect: 3,
};

module.exports = userStatus;
