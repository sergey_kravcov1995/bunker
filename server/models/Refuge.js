const bunkerData = require('../db/bunker-data.json');
const random = require('../utils/random');

class Refuge {
    constructor() {
        this.generateCataclysms();
        this.generateSurvivors();
        this.generateDuration();
        this.generateArea();
        this.generateThings();
    }

    generateCataclysms() {
        const cataclysmIndex = Math.floor(Math.random() * bunkerData.cataclysms.length);
        this.description = bunkerData.cataclysms[cataclysmIndex].description;
    }

    generateSurvivors() {
        this.survivorsNumber = random.getIntInRange(bunkerData.survivors.min, bunkerData.survivors.max);
    }

    generateDuration() {
        this.duration = random.getIntInRange(bunkerData.duration.min, bunkerData.duration.max);
    }

    generateArea() {
        const areaIndex = Math.floor(Math.random() * bunkerData.area.length);
        this.area = bunkerData.area[areaIndex];
    }

    generateThings() {
        this.things = [];
        let thingsNumber = random.getIntInRange(bunkerData.thingNumber.min, bunkerData.thingNumber.max);
        while(thingsNumber > 0) {
            const thingIndex = Math.floor(Math.random() * bunkerData.things.length);
            const newThing = bunkerData.things[thingIndex];
            if (!this.things.find(thing => thing === newThing)) {
                this.things.push(newThing);
                thingsNumber--;
            }
        }
    }
}

module.exports = Refuge;
