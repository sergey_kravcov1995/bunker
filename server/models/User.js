const BaseUser = require('./BaseUser');
const Personage = require('./Personage');

class User extends BaseUser {
    constructor(connectionId, username, roomId = '') {
        super(connectionId, username, roomId);
        this.personage = null;
    }

    generatePersonage() {
        this.personage = new Personage();
    }

    setColor(userIndex) {
        const colors = [
            '#e9967a', '#888', '#a381d8', '#ffffff', '#ffeb3b', '#009000', '#e91e63', '#1e90ff', '#8b0000', '#ff5722',
            '#00bcd4', '#47f9a7', '#9c27b0', '#a9894f',
        ];
        const colorIndex = (userIndex - 1) % colors.length;
        this.color = colors[colorIndex];
    }
}

module.exports = User;
