const BaseRoom = require('./BasePlayroom');
const Refuge = require('./Refuge');

class Room extends BaseRoom {
    constructor(creator, password) {
        super(creator, password);
        this.initGameStatistics();
    }

    initGameStatistics() {
        this.roundNumber = 0; // ToDo
        this.isEveryoneReady = false;
        this.refuge = {};
    }

    startGame() {
        this.isEveryoneReady = true;
        this.refuge = new Refuge();
        this.users.forEach(user => {
            user.generatePersonage();
        });
    }

    addUser(user) {
        super.addUser(user);
        user.setColor(this.users.length);
    }
}

module.exports = Room;