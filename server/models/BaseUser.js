const crypto = require('crypto');

const userStatus = require('../enums/userStatus');

class BaseUser {
    constructor(connectionId, username, roomId = '') {
        this.id = crypto.randomBytes(20).toString('hex');
        this.username = username;
        this.roomId = roomId;
        this.connectionId = connectionId;
        this.startDisconnectTime = null;
        
        this.status = userStatus.online;
        this.ready = false;
    }

    changeUsername(newUsername) {
        this.username = newUsername;
    }

    reconnect(connectionId) {
        this.connectionId = connectionId;
        this.status = userStatus.online;
        this.startDisconnectTime = null;
    }

    disconnect() {
        this.status = userStatus.disconnect;
    }

    refresh() {
        this.startDisconnectTime = Date.now();
        this.status = userStatus.refresh;
    }
}

module.exports = BaseUser;
