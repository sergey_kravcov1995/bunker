const crypto = require('crypto');

const userStatus = require('../enums/userStatus');

class BaseRoom {
    constructor(creator, password) {
        this.id = crypto.randomBytes(20).toString('hex');
        this.password = password;
        this.users = [];
        this.startRemoveTime = null;
        this.addCreator(creator);
    }

    addCreator(creator) {
        this.creator = creator;
        this.addUser(creator);
    }

    addUser(user) {
        user.roomId = this.id;
        this.users.push(user);
    }

    findUserById(userId) {
        return this.users.find(user => user.id === userId);
    }

    findUserByUsername(username) {
        return this.users.find(user => user.username === username);
    }

    disconnectUser(user, isWithLeave = true) {
        if (user.id === this.creator.id) {
            this.creator = this.users.find(u => u.id !== user.id && u.status !== userStatus.disconnect);
        }
        if (isWithLeave) {
            this.users = this.users.filter(u => u.id !== user.id);
        } else {
            user.disconnect();
        }
    }

    isActive() {
        return this.users.length && this.users.find(user => user.status !== userStatus.disconnect)
    }
}

module.exports = BaseRoom;
