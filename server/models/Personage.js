const characterData = require('../db/character-data.json');
const random = require('../utils/random');

class Personage {
    constructor() {
        this.generateBiologicalTraits();
        this.generateProfession();
        this.generateHobby();
        this.generatePhobia();
        this.generateAdditionalInformation();
        this.generateBaggage();
        this.generateFirstActionCard();
        this.generateSecondActionCard();
        this.generateCharacter();
        this.generatePhysique();
        this.generateHealthStatuses();
    }

    generateBiologicalTraits() {
        const opened = this.biologicalTraits ? this.biologicalTraits.opened : false;
        this.biologicalTraits = {
            opened,
        };
        this.generateGender();
        this.generateAge();
        this.generateAttitudeTowardsChildren();
        this.generateSexualOrientation();
    }

    generateGender() {
        const genderIndex = Math.floor(Math.random() * characterData.genderRoles.length);
        this.biologicalTraits.gender = characterData.genderRoles[genderIndex];
    }

    generateAge() {
        this.biologicalTraits.age = random.getIntInRange(characterData.age.min, characterData.age.max);
    }

    generateAttitudeTowardsChildren() {
        this.biologicalTraits.isChildfree = random.getBooleanWithChance(characterData.childfree.chance);
    }

    generateSexualOrientation() {
        const sexualOrientation = random.getFeature(characterData.sexualOrientation);
        this.biologicalTraits.sexualOrientation = sexualOrientation.description;
    }

    generateProfession() {
        const professionIndex = Math.floor(Math.random() * characterData.professions.length);
        const opened = this.profession ? this.profession.opened : false;
        this.profession = {
            description: characterData.professions[professionIndex],
            opened,
        };
    }

    generateHobby() {
        const hobbyIndex = Math.floor(Math.random() * characterData.hobby.length);
        const opened = this.hobby ? this.hobby.opened : false;
        this.hobby = {
            description: characterData.hobby[hobbyIndex],
            opened,
        };
    }

    generatePhobia() {
        const phobia = random.getFeature(characterData.phobias);
        const opened = this.phobia ? this.phobia.opened : false;
        this.phobia = {
            description: phobia.description,
            opened,
        };
    }

    generateAdditionalInformation() {
        const additionalInformationIndex = Math.floor(Math.random() * characterData.additionalInformation.length);
        const opened = this.additionalInformation ? this.additionalInformation.opened : false;
        this.additionalInformation = {
            description: characterData.additionalInformation[additionalInformationIndex],
            opened,
        };
    }

    generateBaggage() {
        const baggageIndex = Math.floor(Math.random() * characterData.baggage.length);
        const opened = this.baggage ? this.baggage.opened : false;
        this.baggage = {
            description: characterData.baggage[baggageIndex],
            opened,
        };
    }

    generateFirstActionCard() {
        const actionCardIndex = Math.floor(Math.random() * characterData.actionCards.length);
        const opened = this.firstActionCard ? this.firstActionCard.opened : false;
        this.firstActionCard = {
            ...characterData.actionCards[actionCardIndex],
            opened,
        };
    }

    generateSecondActionCard() {
        let actionCardIndex;
        while(true) {
            actionCardIndex = Math.floor(Math.random() * characterData.actionCards.length);
            if (characterData.actionCards[actionCardIndex].key !== this.firstActionCard.key) {
                break;
            }
        }
        const opened = this.secondActionCard ? this.secondActionCard.opened : false;
        this.secondActionCard = {
            ...characterData.actionCards[actionCardIndex],
            opened,
        };
    }

    generateCharacter() {
        const characterIndex = Math.floor(Math.random() * characterData.characters.length);
        const opened = this.character ? this.character.opened : false;
        this.character = {
            description: characterData.characters[characterIndex],
            opened,
        };
    }

    generatePhysique() {
        const growth = random.getIntInRange(characterData.growth.min, characterData.growth.max);
        const physique = random.getFeature(characterData.physiques);
        let BMI = random.getIntInRange(physique.min * 100, physique.max * 100) / 100;
        const weight = Math.floor(growth * growth * BMI / 10000);
        const opened = this.character ? this.character.opened : false;
        this.physique = {
            opened,
            growth,
            weight,
            BMI,
            description: physique.description,
        };
    }

    generateHealthStatuses() {
        const healthStatus = random.getFeature(characterData.healthStatuses);
        const opened = this.healthStatus ? this.healthStatus.opened : false;
        const stage = healthStatus.noStage ? 0 : random.getIntInRange(1, 10) * 10;
        this.healthStatus = {
            description: healthStatus.description,
            stage,
            opened,
        };
    }
}

module.exports = Personage;
