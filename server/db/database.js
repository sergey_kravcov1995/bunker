const db = {
    rooms: [],
    users: [],
    findRoom(roomId) {
        return this.rooms.find(room => room.id === roomId);
    },
    findUserById(userId) {
        return this.users.find(user => user.id === userId);
    },
    findUserByConnectionId(connectionId) {
        return this.users.find(user => user.connectionId === connectionId);
    },
};

module.exports = db;
