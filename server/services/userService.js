const db = require('../db/database');
const debconsole = require('../utils/debconsole');
const responseWithError = require('../utils/responseWithError');
const responseStatus = require('../enums/responseStatus');
const userStatus = require('../enums/userStatus');
const validationErrors = require('../resources/validationErrors.json');
const SocketService = require('./SocketService');
const Room = require('../models/Playroom');
const User = require('../models/User');

const minNumberLength = 2;
const maxNumberLength = 14;

class UserService {
    constructor(socket) {
        this.socketService = new SocketService(socket);
        this.currentUser = null;
        this.currentRoom = null;
    }
    
    validationUsername(username) {
        return !username || username.length < 3 || username.length > 15;
    }

    reestablishConnect(data, callback) {
        const room = db.findRoom(data.roomId);
        if (!room) {
            callback(responseWithError(responseStatus.accessError, validationErrors.noRoom));
            return;
        }

        const user = room.findUserById(data.id);
        if (!user) {
            callback(responseWithError(responseStatus.notAuthorized, validationErrors.notAuthorized));
            return;
        }

        this.connectUser(user, room);
    }
    
    reconnectingToRoom(user, data, callback) {
        debconsole('reconnectingToRoom');
        const room = db.findRoom(user.roomId);
        if (room.password !== data.password) {
            callback(responseWithError(responseStatus.wrongPassword, validationErrors.wrongPassword));
            return;
        }

        if (user.username !== data.username) {
            if (room.findUserByUsername(data.username)) {
                callback(responseWithError(responseStatus.validationError, validationErrors.repeatUsername));
                return;
            }
            
            user.changeUsername(data.username);
        }

        this.connectUser(user, room);
    }

    connectUser(user, room) {
        this.currentUser = user;
        this.currentRoom = room;
        const socketId = this.socketService.getSocketId();
        user.reconnect(socketId);
        this.socketService.connected(user, room);

        if (user.status === userStatus.disconnect) {
            this.socketService.reconnectedUser(user, room);
        }
    }

    joinUser(data, callback) {
        if (this.validationUsername(data.username)) {
            callback(responseWithError(responseStatus.validationError, validationErrors.usernameLength));
            return;
        }

        if (data.id) {
            const user = db.findUserById(data.id);
            if (user && user.roomId === data.roomId) {
                this.reconnectingToRoom(user, data, callback);
                return;
            }
        }

        this.createUser(data, callback);
    }
    
    createUser(data, callback) {
        debconsole('createUser');
        const socketId = this.socketService.getSocketId();
        const user = new User(socketId, data.username);
        if (data.roomId) {
            this.joinUserToRoom(data, user, callback);
        } else {
            this.currentUser = user;
            this.currentRoom = new Room(user, data.password);
            // ToDo for test
            /*for (let i = 0; i < 13; i++) {
                const user1 = new User('', data.username);
                user1.ready = true;
                this.currentRoom.addUser(user1);
            }*/
            db.rooms.push(this.currentRoom);
            db.users.push(user);
            this.socketService.connected(user, this.currentRoom);
        }
    }
    
    joinUserToRoom(data, user, callback) {
        debconsole('joinUserToRoom');
        const room = db.findRoom(data.roomId);
        if (!room) {
            callback(responseWithError(responseStatus.accessError, validationErrors.noRoom));
            return;
        }

        if (room.password !== data.password) {
            callback(responseWithError(responseStatus.wrongPassword, validationErrors.wrongPassword));
            return;
        }

        if (room.isEveryoneReady) {
            callback(responseWithError(responseStatus.accessError, validationErrors.gameStarted));
            return;
        }

        if (room.users.length >= maxNumberLength) {
            callback(responseWithError(responseStatus.accessError, validationErrors.maxNumberLength));
            return;
        }

        if (room.findUserByUsername(user.username)) {
            callback(responseWithError(responseStatus.validationError, validationErrors.repeatUsername));
            return;
        }
        
        db.users.push(user);
        room.addUser(user);
        this.currentUser = user;
        this.currentRoom = room;
        this.socketService.connected(user, room);
        this.socketService.joinedNewUser(user);
    }

    completeDisconnectUser(isFullDisconnect) {
        debconsole("A user disconnected");
        this.currentRoom.disconnectUser(this.currentUser, isFullDisconnect);
        if (isFullDisconnect) {
            debconsole(`user ${currentUser.username} removed`);
            db.users = db.users.filter(user => user.id !== this.currentUser.id);
        }

        if (this.currentRoom.isActive()) {
            this.socketService.disconnectedUser(this.currentUser.username, this.currentRoom);
        } else {
            this.removeRoom();
        }
    }

    removeRoom() {
        debconsole(`removeRoom ${this.currentRoom.id} ?`);
        const interval = 20000;
        this.currentRoom.startRemoveTime = Date.now();
        setTimeout(() => {
            if (!this.currentRoom.isActive() && this.currentRoom.startRemoveTime + interval <= Date.now()) {
                db.users = db.users.filter(user => user.roomId !== this.currentRoom.id);
                db.rooms = db.rooms.filter(room => room.id !== this.currentRoom.id);
            }
        }, interval);
    }

    disconnectUser() {
        if (this.currentUser) {
            this.currentUser.refresh();
            const interval = 10000;
            setTimeout(() => {
                if (this.currentUser.status !== userStatus.online && this.currentUser.startDisconnectTime + interval <= Date.now()) {
                    this.completeDisconnectUser(false);
                }
            }, interval);
        }
    }

    readiness() {
        if (this.currentRoom.users.length >= minNumberLength) {
            this.currentUser.ready = !this.currentUser.ready;
            this.socketService.readiness(this.currentUser.id);
            if (!this.currentRoom.users.find(user => !user.ready)) {
                this.currentRoom.startGame();
                this.socketService.fullReadiness(this.currentRoom);
            }
        }
    }
}

module.exports = UserService;
