class SocketService {
    constructor(socket) {
        this.socket = socket;
    }
    
    getSocketId() {
        return this.socket.id;
    }
    
    join(roomId) {
        this.roomId = roomId;
        this.socket.join(roomId);
    }
    
    connected(user, room) {
        this.join(room.id);
        this.socket.emit('connected', {
            user,
            room,
        });
    }
    
    reconnectedUser(user, room) {
        this.socket.broadcast.to(this.roomId).emit('reconnectedUser', { // ToDo no use
            user,
            room,
        });
    }
    
    joinedNewUser(user) {
        this.socket.broadcast.to(this.roomId).emit('joinedNewUser', user);
    }

    disconnectedUser(username, room) {
        this.socket.broadcast.to(this.roomId).emit('disconnected-user', { // ToDo no use
            username,
            room,
        });
    }
    
    readiness(userId) {
        this.socket.broadcast.to(this.roomId).emit('readiness', userId);
    }
    
    fullReadiness(room) {
        this.socket.broadcast.to(this.roomId).emit('fullReadiness', room);
        this.socket.emit('fullReadiness', room);
    }
}

module.exports = SocketService;
