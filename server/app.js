const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const UserService = require('./services/UserService');
const debconsole = require('./utils/debconsole');

io.on('connection', (socket) => {
    debconsole("connection");
    const userService = new UserService(socket);

    socket.on('leave', () => {
        debconsole("leave");
        userService.completeDisconnectUser(true);
    });

    socket.on('join', (data, callback) => {
        debconsole('join', data);
        userService.joinUser(data, callback);
    });

    socket.on('disconnect', () => {
        debconsole('disconnect');
        userService.disconnectUser();
    });

    socket.on('reestablish-connect', (data, callback) => {
        debconsole("reestablish-connect");
        userService.reestablishConnect(data, callback);
    });

    socket.on('readiness', () => {
        debconsole("readiness");
        userService.readiness();
    });
});

module.exports = {
    app,
    server,
};
