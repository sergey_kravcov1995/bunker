const random = {
    getIntInRange: (min, max) => Math.floor(Math.random() * (max - min + 1)) + min,
    getBooleanWithChance: (chance) => Math.floor(Math.random() * 100) < chance,
    getFeature: (features) => {
        let result = Math.floor(Math.random() * 100);
        return features.find(feature => {
            result -= feature.chance;
            return result <= 0;
        }) || features[features.length - 1];
    },
};

module.exports = random;
