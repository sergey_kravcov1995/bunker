const responseWithError = (status, errorMessage) => ({ status, errorMessage });

module.exports = responseWithError;
