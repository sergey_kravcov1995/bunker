export default {
    props: {
        refuge: {
            type: Object,
            required: true,
        },
    },
    methods: {
        selectDossier() {
            this.$emit('selectDossier', null);
        },
    },
}
