import userStatus from '~/client-enums/userStatus';

export default {
    props: {
        user: {
            type: Object,
            required: true,
        },
    },
    computed: {
        avatar() {
            if (this.user) {
                if (this.user.status === userStatus.disconnect) {
                    return 'person_remove';
                }
                if (this.user.status === userStatus.disconnect) {
                    return 'person_outline';
                }
                if (this.user.ready) {
                    return 'how_to_reg';
                }
                return 'person';
            }
        },
        userInfStyle() {
            return {
                color: this.user.color,
            };
        },
    },
}
