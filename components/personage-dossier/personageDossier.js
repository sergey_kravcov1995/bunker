import DossierFeature from '~/components/dossier-feature/dossierFeature.vue';

export default {
    components: {
        DossierFeature,
    },
    props: {
        user: {
            type: Object,
            required: true,
        },
        dossierIndex: {
            type: Number,
            required: true,
        },
    },
    computed: {
        isSelf() {
            return this.dossierIndex === 0;
        },
        personage() {
            return this.user.personage;
        },
        childfreeText() {
            return this.personage.biologicalTraits.isChildfree
                ? 'Чайлдфри'
                : '-';
        },
        healthStatusText() {
            return this.personage.healthStatus.stage
                ? `${this.personage.healthStatus.description} (${this.personage.healthStatus.stage}%)`
                : this.personage.healthStatus.description;
        },
        tagStyles() {
            const top = this.dossierIndex * 50;
            return {
                top: `${top}px`,
                'background-color': this.user.color,
                'border-color': this.user.color,
            };
        },
        tagAngleStyles() {
            return {
                'border-left-color': this.user.color,
            };
        },
        colorStyle() {
            return {
                color: this.user.color,
            };
        },
    },
    methods: {
        selectDossier() {
            this.$emit('selectDossier', this.user.id);
        },
    },
}
