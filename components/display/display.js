export default {
    props: {
        playerCount: {
            type: Number,
            required: true,
        },
    },
    computed: {
        roomId() {
            return this.$store.state.room.id;
        },
        label() {
            return `Ключ: ${this.roomId}`;
        },
        isReady: {
            get() {
                return this.$store.state.user.ready;
            },
            set() {
                this.$socket.emit('readiness');
                this.$store.commit('readiness');
            }
        },
        isFewPeople() {
            return this.playerCount < this.minNumberPlayer;
        },
        isFullRoom() {
            return this.playerCount >= this.maxNumberPlayer;
        },
    },
    created() {
        this.minNumberPlayer = 2;
        this.maxNumberPlayer = 14;
    },
    methods: {
        copyKey(event) {
            navigator.clipboard.writeText(this.label);
            event.currentTarget.blur();
        },
    },
}
