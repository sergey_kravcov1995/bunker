export default {
    props: {
        isSelf: {
            type: Boolean,
            required: true,
        },
        opened: {
            type: Boolean,
            required: true,
        },
        description: {
            type: String|Number,
            required: true,
        },
    },
    computed: {
        isHidden() {
            return !this.isSelf && !this.opened;
        },
        text() {
            return this.isHidden
                ? '***'
                : this.description;
        },
    },
}
