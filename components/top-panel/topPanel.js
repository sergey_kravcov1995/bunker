import UserInf from '~/components/user-inf/userInf.vue';

export default {
    components: {
        UserInf,
    },
    computed: {
        user() {
            return this.$store.state.user;
        },
        otherPlayers() {
            return this.$store.state.room && this.$store.state.room.users
                ? this.$store.state.room.users.filter(user => user.id !== this.user.id)
                : [];
        },
    },
}
