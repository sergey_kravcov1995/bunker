export const state = () => ({
    user: {},
    room: {},
});

export const mutations = {
    readiness(state) {
        state.user.ready = !state.user.ready;
    },
    SOCKET_connected(state, data) {
        state.room = data.room;
        state.user = data.room.users.find(user => user.id === data.user.id);
    },
    SOCKET_joinedNewUser(state, user) {
        state.room.users.push(user);
    },
    SOCKET_readiness(state, userId) {
        const user = state.room.users.find(u => u.id === userId);
        user.ready = !user.ready;
    },
    SOCKET_fullReadiness(state, room) {
        state.room = room;
        state.user = room.users.find(user => user.id === state.user.id);
    },
};
