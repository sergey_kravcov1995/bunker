import responseStatus from '~/client-enums/responseStatus';

export default {
    head: {
        title: 'Бункер! Авторизация'
    },
    data() {
        return {
            id: '',
            username: '',
            room: '',
            password: '',
            isValidationIncluded: false,
            roomErrorMessage: '',
            usernameErrorMessage: '',
            passwordErrorMessage: '',
        };
    },
    computed: {
        isUsernameValid() {
            return !this.isValidationIncluded || (this.username && this.username.length >= 3 && this.username.length <= 15);
        },
        isDisabledLoginButton() {
            return !this.isUsernameValid || !!this.roomErrorMessage || !!this.usernameErrorMessage || !!this.passwordErrorMessage;
        },
    },
    watch: {
        room() {
            this.roomErrorMessage = '';
        },
        password() {
            this.passwordErrorMessage = '';
        },
        username() {
            this.usernameErrorMessage = '';
        },
    },
    created() {
        if(process.browser){
            this.id = localStorage.id || '';
            this.username = localStorage.username || '';
            this.room = this.$route.query.room || '';
        }
    },
    methods: {
        login() {
            this.isValidationIncluded = true;
            if (this.isUsernameValid) {
                const requestData = {
                    id: this.id,
                    username: this.username,
                    roomId: this.room,
                    password: this.password,
                };
                this.$socket.emit('join', requestData, (responseData) => {
                    if (responseData.status === responseStatus.accessError) {
                        this.roomErrorMessage = responseData.errorMessage;
                    } else if (responseData.status === responseStatus.wrongPassword) {
                        this.passwordErrorMessage = responseData.errorMessage;
                    } else {
                        this.usernameErrorMessage = responseData.errorMessage;
                    }
                });
                this.isValidationIncluded = false;
            }
        },
    },
    sockets: {
        connected(responseData) {
            if (process.browser) {
                localStorage.setItem('username', responseData.user.username);
                if (this.id !== responseData.user.id) {
                    this.id = responseData.user.id;
                    localStorage.setItem('id', this.id);
                }
                
                this.$router.push({ name: 'playroom-id', params: { id: responseData.room.id } });
            }
        },
    },
}
