import responseStatus from '~/client-enums/responseStatus';
import PersonageDossier from '~/components/personage-dossier/personageDossier.vue';
import RefugeDossier from '~/components/refuge-dossier/refugeDossier.vue';
import Display from '~/components/display/display.vue';

export default {
    layout: 'default/default',
    head: {
        title: 'Бункер',
    },
    components: {
        PersonageDossier,
        RefugeDossier,
        Display,
    },
    data() {
        return {
            selectedUser: null,
        };
    },
    computed: {
        user() {
            return this.$store.state.user;
        },
        personage() {
            return this.$store.state.user.personage;
        },
        otherPlayers() {
            return this.$store.state.room && this.$store.state.room.users
                ? this.$store.state.room.users.filter(user => user.id !== this.user.id)
                : [];
        },
        playerCount() {
            return this.$store.state.room && this.$store.state.room.users
                ? this.$store.state.room.users.length
                : 0;
        },
        isGameStarted() {
            return this.$store.state.room.isEveryoneReady;
        },
        refuge() {
            return this.$store.state.room.refuge;
        },
    },
    created() {
        this.minNumberPlayer = 2;
        if (!this.user || !this.user.id) {
            this.checkSavedUser();
        }
    },
    methods: {
        checkSavedUser() {
            if(process.browser) {
                const id = localStorage.id || '';
                const username = localStorage.username || '';
                const roomId = this.$route.params.id;
                if (id && username) {
                    this.reestablish({
                        username,
                        id,
                        roomId,
                    });
                } else {
                    this.$router.push({ name: 'index', query: { room: roomId } });
                }
            }
        },
        reestablish(data) {
            this.$socket.emit('reestablish-connect', data, (responseData) => {
                if (responseData.status === responseStatus.accessError) {
                    this.$router.push({ name: 'index' });
                } else if (responseData.status === responseStatus.notAuthorized)  {
                    this.$router.push({ name: 'index', query: { room: data.roomId } });
                }
            });
        },
        selectDossier(userId) {
            this.selectedUser = userId;
        },
    },
}
