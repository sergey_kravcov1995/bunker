const responseStatus = {
    ok: 0,
    validationError: 1,
    accessError: 2,
    notAuthorized: 3,
    wrongPassword: 4,
};

export default responseStatus;
