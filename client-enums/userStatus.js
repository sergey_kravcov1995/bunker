const userStatus = {
    online: 1,
    refresh: 2,
    disconnect: 3
};

export default userStatus;
