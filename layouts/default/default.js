import TopPanel from '~/components/top-panel/topPanel.vue';

export default {
    components: {
        TopPanel,
    },
    computed: {
        user() {
            return this.$store.state.user;
        },
    },
}
